#!/usr/bin/env bash
PYTHONPATH=$MU_HOLSTEIN/migrate_scripts python3 $PYPY_MU/rpython/translator/mu/tool/gen_rmu.py --impl ref $MU_HOLSTEIN/cbinding/muapi.h > $PYPY_MU/rpython/rlib/rmu/holstein.py
PYTHONPATH=$MU_HOLSTEIN/migrate_scripts python3 $PYPY_MU/rpython/translator/mu/tool/gen_rmu.py --impl fast $MU_ZEBU/src/vm/api/muapi.h > $PYPY_MU/rpython/rlib/rmu/zebu.py
PYTHONPATH=$MU_HOLSTEIN/migrate_scripts python3 $PYPY_MU/rpython/translator/mu/tool/gen_rmu_c.py --impl ref $MU_HOLSTEIN/cbinding/muapi.h > $PYPY_MU/rpython/rlib/rmu/holstein_c.py
PYTHONPATH=$MU_HOLSTEIN/migrate_scripts python3 $PYPY_MU/rpython/translator/mu/tool/gen_rmu_c.py --impl fast $MU_ZEBU/src/vm/api/muapi.h > $PYPY_MU/rpython/rlib/rmu/zebu_c.py
