from rpython.rtyper.lltypesystem import rffi
from rpython.rlib.rposix import c_write

def print_(s):
    """
    Directly call c_write without all the stuff in StdOutBuffer,
    this can considerably shorten execution trace.
    :param s:
    :return:
    """
    s += '\n'
    c_write(rffi.cast(rffi.INT, 1),
            rffi.cast(rffi.CCHARP, rffi.str2charp(s)),
            rffi.cast(rffi.SIZE_T, len(s)))