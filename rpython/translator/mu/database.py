from rpython.flowspace.model import Constant
from rpython.rtyper.lltypesystem import lltype, rffi
from rpython.translator.mu import mutype
from rpython.translator.platform import platform
from rpython.tool.udir import udir
from rpython.config.translationoption import get_translation_config

import ctypes, ctypes.util
import os, sys, py, re

from rpython.tool.ansi_mandelbrot import Driver
from rpython.tool.ansi_print import AnsiLogger
log = AnsiLogger("MuDatabase")
mdb = Driver()


class MuDatabase:
    def __init__(self, tlc):
        # type: (rpython.translator.translator.TranslationContext) -> None
        self.tlc = tlc
        self.graphs = tlc.graphs
        self.types = set()
        self.consts = set()
        self.funcref_consts = set()
        self.gcells = set()
        self.extern_fncs = set()
        self.objtracer = None
        self.libsupport_path = None
        self.merged_eci = None
        self.mu_name_map = {}
        self.nman = None
        self.heap_NULL_constant_map = {}

    def build_database(self):
        """
        Tasks to be done at database stage:
        - lower the debug operations to mu_ccalls
        - collect all global definitions
            - types
            - constants
            - external functions
            - global cells
            - graphs & function references
        - assign a Mu name to each global entity and local variable
        - process external C functions
            - compiling C function macros,
                        C function declared in post_include_bits,
                        and C functions defined in PyPy C backend into one shared library
            - update the ecis, and rename function names
        - trace heap objects
        """
        # TODO: lower debug operations
        self.collect_global_defs()
        self.compile_pypy_c_extern_funcs()
        self.assign_mu_name()

    def collect_global_defs(self):
        # collect global definitions in graphs
        for graph in self.graphs:
            self._add_type(graph.sig)

            for blk in graph.iterblocks():
                for a in blk.inputargs:
                    self._add_type(a.concretetype)
                if blk.mu_excparam:
                    self._add_type(blk.mu_excparam.concretetype)

                for op in blk.operations:
                    for a in op.args:
                        self._add_type(a.concretetype)
                        if isinstance(a, Constant):
                            self._collect_constant(a)
                    self._add_type(op.result.concretetype)
                for lnk in blk.exits:
                    for a in lnk.args:
                        self._add_type(a.concretetype)
                        if isinstance(a, Constant):
                            self._collect_constant(a)
                    if isinstance(lnk.exitcase, Constant):
                        self._collect_constant(lnk.exitcase)

        # trace heap objects
        self.objtracer = HeapObjectTracer()

        for gcl in self.gcells:
            self.objtracer.trace(gcl.value._load())

        # add types in heap to global type definitions
        for t in self.objtracer.types_in_heap():
            self._add_type(t)

        # create constants for NULLs found in heap objects
        for T in self.objtracer.nullref_ts:
            c = Constant(T._null(), T)
            self.consts.add(c)
            self.heap_NULL_constant_map[T] = c

    def _collect_constant(self, c):
        if isinstance(c.concretetype, mutype.MuNumber):
            self.consts.add(c)
        elif isinstance(c.concretetype, mutype.MuGlobalCell):
            self.gcells.add(c)
        elif isinstance(c.concretetype, mutype.MuReferenceType) and c.value._is_null():
            self.consts.add(c)
        elif isinstance(c.concretetype, mutype.MuUFuncPtr):
            self.extern_fncs.add(c)
        elif isinstance(c.concretetype, mutype.MuFuncRef):
            self.funcref_consts.add(c)

    def _add_type(self, T):
        assert isinstance(T, mutype.MuType)

        if isinstance(T, mutype.MuGlobalCell):
            T = T.TO

        if T in self.types:
            return

        self.types.add(T)

        if isinstance(T, mutype.MuStruct):
            for FLD in tuple(getattr(T, fld) for fld in T._names):
                self._add_type(FLD)
        elif isinstance(T, mutype.MuHybrid):
            for FLD in tuple(getattr(T, fld) for fld in T._names[:-1]):
                self._add_type(FLD)
            self._add_type(T._vartype.OF)
        elif isinstance(T, mutype.MuArray):
            self._add_type(T.OF)
        elif isinstance(T, mutype.MuObjectRef):
            self._add_type(T.TO)
            if T == mutype.MU_WEAKREF_VOID:
                self._add_type(mutype.MuRef(mutype.MU_VOID))    # add ref<void> so help to declare NULL
        elif isinstance(T, mutype.MuGeneralFunctionReference):
            self._add_type(T.Sig)
        elif isinstance(T, mutype.MuFuncSig):
            ts = T.ARGS + T.RESULTS
            for t in ts:
                self._add_type(t)

    def compile_pypy_c_extern_funcs(self):
        all_ecis = []
        replace_ecis = []
        header_file_name = 'common_header.h'
        header_file_dir_path = udir.strpath

        macro_wrapper_regex = re.compile(r'RPY_EXTERN (?P<ret_t>.*) '
                                         r'\*?pypy_macro_wrapper_(?P<macro_name>[\w_]*)'
                                         r'\((?P<arg_list>.*)\) '
                                         r'\{ *(return)? *(?P=macro_name)\(.*\); \}')

        # step 1: identify macros, functions defined in post_include_bits
        for c in self.extern_fncs:
            fnp = c.value
            eci = fnp.eci

            if eci.post_include_bits:
                if any(fnp._name in s for s in eci.post_include_bits):      # C function declaration
                    # wrap in a macro (renaming will be done below)
                    # rely on clang -O3 optimisation to inline
                    _macro_fnp = rffi.llexternal(fnp._name, fnp._llfnctype.ARGS, fnp._llfnctype.RESULT,
                                                 compilation_info=eci,
                                                 macro=True, _nowrapper=True)
                    eci = _macro_fnp._obj.compilation_info

            if hasattr(eci, '_with_ctypes'):
                # function in the same module with macros (same eci)
                eci = eci._with_ctypes
                fnp.eci = eci   # reassign the eci
                for src_str in eci.separate_module_sources:
                    m = re.match(macro_wrapper_regex, src_str)
                    if m:
                        d = m.groupdict()
                        if fnp._name == d['macro_name']:    # is a macro
                            fnp._name = 'pypy_macro_wrapper_' + fnp._name
                            break
                    else:
                        # print src_str
                        pass

            if eci.post_include_bits or eci.separate_module_sources or eci.separate_module_files:
                replace_ecis.append(fnp)

            all_ecis.append(eci)

        # HACK: define LL_stack_set_length_fraction to be an empty function
        hack_eci = rffi.ExternalCompilationInfo(
            separate_module_sources=['RPY_EXTERN void LL_stack_set_length_fraction(double fraction) {}']
        )
        all_ecis.append(hack_eci)

        pypy_include_dir = py.path.local(__file__).join('..', '..', 'c')
        eci = rffi.ExternalCompilationInfo(include_dirs=[pypy_include_dir.strpath])
        eci = eci.merge(*all_ecis).convert_sources_to_files()

        # step 2: convert all separate module sources to files
        header_file = udir.join(header_file_name)
        with header_file.open('w') as fp:
            fp.write('#ifndef _PY_COMMON_HEADER_H\n#define _PY_COMMON_HEADER_H\n')
            eci.write_c_header(fp)
            fp.write('#include "src/g_prerequisite.h"\n')
            fp.write('#endif /* _PY_COMMON_HEADER_H*/\n')

        # add common header to eci
        eci.post_include_bits = ()  # should have been written to mu_common_header.h
        eci = eci.merge(rffi.ExternalCompilationInfo(includes=[header_file_name], include_dirs=[header_file_dir_path]))

        # step 3: compile these files into shared library
        eci = eci.compile_shared_lib(platform.so_prefixes[0] + 'pypy_mu_support',
                                     debug_mode=False,      # no '-g -O0'
                                     defines=['RPY_EXTERN=RPY_EXPORTED'])

        # step 4: update eci to include the compiled shared library
        for fnp in replace_ecis:
            fnp.eci = eci

        if eci.libraries:
            # NOTE: move compiled library to be under current dir
            libpath = py.path.local(eci.libraries[-1])
            suplibdir = get_translation_config().translation.mu.suplibdir
            targetpath = py.path.local(suplibdir).join(libpath.basename)
            libpath.move(targetpath)
            self.libsupport_path = targetpath
            eci.libraries = eci.libraries[:-1] + (targetpath.strpath, )

        self.merged_eci = eci
        self.extern_fncs = set(list(self.extern_fncs))  # rehash all
        return eci

    def assign_mu_name(self):
        man = MuNameManager()
        self.nman = man
        # types
        for T in self.types:
            self.mu_name_map[T] = man.assign(T)

        # constants
        for c in self.consts:
            self.mu_name_map[c] = man.assign(c)

        for c in self.extern_fncs:
            self.mu_name_map[c] = man.assign(c)

        # global cells
        for c in self.gcells:
            self.mu_name_map[c] = man.assign(c)

        # graphs
        for g in self.tlc.graphs:
            graph_name = man.get_graph_name(g)
            g.name = graph_name[1:]     # without @
            self.mu_name_map[g] = graph_name
            for i, blk in enumerate(g.iterblocks()):
                blk_name = '%(graph_name)s.blk%(i)d' % locals()
                self.mu_name_map[blk] = blk_name

                for v in blk.inputargs:
                    self.mu_name_map[v] = '%(blk_name)s.%(v)s' % locals()
                if blk.mu_excparam:
                    ep = blk.mu_excparam
                    self.mu_name_map[ep] = '%(blk_name)s.%(ep)s' % locals()
                for op in blk.operations:
                    res = op.result
                    self.mu_name_map[res] = '%(blk_name)s.%(res)s' % locals()
                    if op.opname == 'mu_binop':
                        metainfo = op.args[-1].value
                        if 'status' in metainfo:
                            for v in metainfo['status'][1]:
                                self.mu_name_map[v] = '%(blk_name)s.%(v)s' % locals()


class HeapObjectTracer:
    def __init__(self):
        self.heap_objs = set()
        self.fixed_objs = set()  # objects pointed to by uptr, needs relocation support
        self.nullref_ts = set()
        self.types = set()

    def trace(self, obj):
        MuT = mutype.mutypeOf(obj)
        if not isinstance(MuT, mutype._MuMemArray):
            self.types.add(MuT)

        if isinstance(MuT, mutype.MuReferenceType) and obj._is_null():
            self.nullref_ts.add(mutype.mutypeOf(obj))
            return

        if isinstance(MuT, mutype.MuObjectRef):
            self.check_reference_assumptions(obj)

            refnt = obj._obj() if isinstance(MuT, mutype.MuWeakRef) else obj._obj
            if isinstance(mutype.mutypeOf(refnt), mutype.MuStruct):
                refnt = refnt._normalizedcontainer()

            obj_set = self.fixed_objs if isinstance(MuT, mutype.MuUPtr) else self.heap_objs
            if refnt not in obj_set:
                obj_set.add(refnt)
                self.trace(refnt)

        elif isinstance(MuT, (mutype.MuStruct, mutype.MuHybrid)):
            for fld in MuT._flds:
                self.trace(getattr(obj, fld))

        elif isinstance(MuT, (mutype._MuMemArray, mutype.MuArray)):
            if isinstance(MuT.OF, (mutype.MuContainerType, mutype.MuObjectRef)):
                for i in range(len(obj.items)):
                    itm = obj[i]
                    self.trace(itm)

    def check_reference_assumptions(self, ref):
        obj = ref._obj
        norm_obj = obj
        if isinstance(obj, mutype._mustruct):
            norm_obj = obj._normalizedcontainer()

        # assumption 1: all references should be to the beginning
        # of containers, not in the guts
        if isinstance(norm_obj, mutype._muparentable):
            assert norm_obj._parentstructure() is None

        # assumption 2: if ref is uptr,
        # then the outer container must not be pointed by a ref
        if isinstance(ref, mutype._muuptr):
            # TODO: how to check this?
            pass

    def types_in_heap(self):
        return self.types


class MuNameManager:
    def __init__(self):
        self.name_map = {
            mutype.MU_FLOAT: '@flt',
            mutype.MU_DOUBLE: '@dbl',
            mutype.MU_VOID: '@void',
        }
        self._counter = {
            'stt': 0,
            'hyb': 0,
            'arr': 0,
            'gcl': 0,
        }
        self._assigned_names = set()
        self._conflict_ctr = {}
        self.graph_name_dic = {}

        # verbose struct name?
        from rpython.config.translationoption import get_translation_config
        config = get_translation_config()
        if config:
            self.verbose_type_name = config.translation.mu.verbtypename

    def assign(self, obj):
        if isinstance(obj, mutype.MuType):
            name = self.get_type_name(obj)
        else:
            assert isinstance(obj, Constant)
            name = self.get_const_name(obj)

            if name in self._assigned_names:
                log.WARNING('name conflict: already assigned %(name)s' % locals())
                n = self._conflict_ctr.get(name, 2)
                self._conflict_ctr[name] = n + 1
                name = "%(name)s_%(n)d" % locals()
                log.WARNING('rename to %(name)s.' % locals())

        self._assigned_names.add(name)

        return name

    def get_type_name(self, MuT):
        if MuT in self.name_map:
            return self.name_map[MuT]

        if isinstance(MuT, mutype.MuIntType):
            name = 'i%d' % MuT.BITS

        if isinstance(MuT, mutype.MuStruct):
            if getattr(self, 'verbose_type_name', False):
                name = 'stt' + MuT._name
                if name in self._counter:
                    self._counter[name] += 1
                    name = name + str(self._counter[name])
                else:
                    self._counter[name] = 1
            else:
                name = 'stt%d' % self._counter['stt']
                self._counter['stt'] += 1

        if isinstance(MuT, mutype.MuHybrid):
            if getattr(self, 'verbose_type_name', False):
                name = 'hyb' + MuT._name
                if name in self._counter:
                    self._counter[name] += 1
                    name = name + str(self._counter[name])
                else:
                    self._counter[name] = 1
            else:
                name = 'hyb%d' % self._counter['hyb']
                self._counter['hyb'] += 1

        if isinstance(MuT, mutype.MuArray):
            if getattr(self, 'verbose_type_name', False):
                name = 'arr%d%s' % (MuT.length, self.get_type_name(MuT.OF)[1:])
                if name in self._counter:
                    self._counter[name] += 1
                    name = name + str(self._counter[name])
                else:
                    self._counter[name] = 1
            else:
                name = 'arr%d' % self._counter['arr']
                self._counter['arr'] += 1

        if isinstance(MuT, mutype.MuReferenceType):
            prefix_map = {
                mutype.MuRef: 'ref',
                mutype.MuIRef: 'irf',
                mutype.MuUPtr: 'ptr',
                mutype.MuWeakRef: 'wrf',
                mutype.MuFuncRef: 'fnr',
                mutype.MuUFuncPtr: 'fnp',
                mutype.MuOpaqueRef: 'opqr'
            }
            prefix = prefix_map[type(MuT)]
            if isinstance(MuT, mutype.MuObjectRef):
                refnt = self.get_type_name(MuT.TO)[1:]
                name = prefix + refnt
            elif isinstance(MuT, mutype.MuGeneralFunctionReference):
                sig = self.get_type_name(MuT.Sig)[1:]
                name = prefix + sig
            elif isinstance(MuT, mutype.MuOpaqueRef):
                name = prefix + MuT.obj_name

        if isinstance(MuT, mutype.MuFuncSig):
            name = 'sig_%(args)s_%(rets)s' % {
                'args': ''.join([self.get_type_name(T)[1:] for T in MuT.ARGS]),
                'rets': ''.join([self.get_type_name(T)[1:] for T in MuT.RESULTS])
            }

        name = '@' + name
        self.name_map[MuT] = name
        return name

    def get_const_name(self, const):
        if isinstance(const.concretetype, mutype.MuGlobalCell):
            name = 'gcl%d' % self._counter['gcl']
            self._counter['gcl'] += 1
        elif isinstance(const.concretetype, mutype.MuReferenceType) and const.value._is_null():
            name = 'NULL_%s' % self.get_type_name(const.concretetype)[1:]
        elif isinstance(const.concretetype, mutype.MuNumber):
            name = '%(hex)s_%(type)s' % {'hex': mutype.hex_repr(const.value),
                                         'type': self.get_type_name(const.concretetype)[1:]}
        elif isinstance(const.concretetype, mutype.MuUFuncPtr):
            name = 'extfnc_' + const.value._name

        return '@' + name

    def get_graph_name(self, g):
        name = g.name if '.' in g.name else g.name.split('__')[0]
        name = re.sub(r'[^0-9a-zA-Z_-]', '_', name)     # replace illegal characters in names
        name_dic = self.graph_name_dic

        if name not in name_dic:
            ctr = 0
            name_dic[name] = ([g], ctr)
        else:
            gs, ctr = name_dic[name]
            if g not in gs:
                gs.append(g)
                ctr += 1
                name_dic[name] = (gs, ctr)
        return "@%s_%d" % (name, ctr)
