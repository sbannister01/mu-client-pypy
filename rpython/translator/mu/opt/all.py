from rpython.translator.backendopt.removenoops import remove_unaryops
from rpython.translator.mu.opt import boolflag

def mu_backend_optimisations(tlc):
    graphs = tlc.graphs

    # remove non-translatable operations that are equivalent to 'same_as'
    for g in graphs:
        remove_unaryops(g, ['hint', 'jit_force_virtual'])

    # remove redundant ZEXT and EQ operations that were added due to scope and type limitations
    boolflag.optimise(graphs)
