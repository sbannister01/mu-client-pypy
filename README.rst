==============================
PyPy-Mu: A Mu Backend for PyPy
==============================
.. image:: https://gitlab.anu.edu.au/mu/mu-client-pypy/badges/mu-rewrite/build.svg
    :target: https://gitlab.anu.edu.au/mu/mu-client-pypy/commits/mu-rewrite

Welcome to PyPy-Mu!

PyPy-Mu is a fork of `PyPy <http://pypy.org>`__ that aims to
add a `Mu Micro Virtual Machine <http://microvm.org>`__ backend for it.

This project is currently under active development,
right now we can compile PyPy interpreter with `--no-allworkingmodules` option.

Building
========

Obtaining a Mu implementation
-----------------------------

The reference implementation (Holstein) for Mu can be found `here <https://gitlab.anu.edu.au/mu/mu-impl-ref2>`__.
Build the Mu implementation, make sure to build the C binding as well.
The fast implementation (Zebu) for Mu can be found `here <https://gitlab.anu.edu.au/mu/mu-impl-fast>`__.
This implementation is still in progress.

Reference implementation is targetted by default.
You can specify which implementation to target using the flag ``--mu-impl=<impl>`` to RPython.


Setting up environment variable
-------------------------------
.. role:: bash(code)
    :language: bash

RPython Mu API binding requires ``$MU_HOLSTEIN`` and/or ``$MU_ZEBU`` environment variable to be set.
You need to set them to point to the cloned Mu implementation repository.

Apply Patch to PyPy/RPython Source Code
---------------------------------------

You may need to also apply the patch to PyPy/RPython source code in fixing some problems.
Some of these changes are acknowledged and committed on the PyPy upstream, some are yet resolved.

::

    $ git apply *.patch
    


Compiling & Executing RPython Target
------------------------------------

Specify :bash:`-b mu` option to compile using the Mu backend:

::

    $ rpython/bin/rpython -b mu <target>

This builds the program bundle and dumpts out a boot image as ``<target>-mu`` in the current directory.
The nature of the boot image is implementation defined.
Currently for Holstein, it is a zip file; for Zebu, it is a binary executable.

Note the default Mu code generation backend is API (building bundle through API calls).
This back-end can be slow for compiling larger RPython programs.
An alternative is caching the calls into C sources, and run from there (remove the overhead of RPython FFI).
To specify the code generation back-end, use the flag ``--mu-codegen=<backend>``, which takes ``c`` or ``api``.


To run the dumped boot image under Holstein, use the `runmu.sh` shell script.

::

    $ $MU_HOLSTEIN/tools/runmu.sh <mu-flags> <bootimage> <program-args>

--------------------------

Why not try compiling the PyPy interpreter (currently with some limitations)?

::

    $ PYTHONPATH=$PYPY_MU pypy rpython/bin/rpython -O3 -b mu --mu-vmargs='sosSize=780M\nlosSize=780M' --mu-codegen=c --no-shared $PYPY_MU/pypy/goal/targetpypystandalone.py --no-allworkingmodules
    $ $MU/tools/runmu.sh --staticCheck=false --sourceInfo=false --vmLog=ERROR --sosSize=780M --losSize=780M $PYPY_MU/pypy/bin/pypy-mu

Note: when building the pypy interpreter, put the boot image under pypy/bin, so that PyPy can initialise the path correctly.

What can I contribute?
======================

Please have a look at `issues <https://gitlab.anu.edu.au/mu/mu-client-pypy/issues>`__,
but in general we need help in following areas:

- Retargetting RPython JIT to Mu
- Adding more RPython level optimizations
- Getting more modules working on Mu (including Holstien and Zebu).

Where can I get help?
=====================

Following sections are kindly borrowed from `mu-client-ghc <https://gitlab.anu.edu.au/mu/mu-client-ghc>`__.

This project is under active development, at present by Zixian Cai (u5937495@anu.edu.au),
to whom you should direct most (code-related) questions.

Previous work was done by John Zhang (John.Zhang@anu.edu.au),
Timm Allman (tallman@cs.umass.edu) and Ben Kushigian (bkushigian@umass.edu) - they may be in a better position to
answer certain questions about some of the existing code, but bear in mind that
they are not currently working on this project and thus generally have other
things to do with their time.

Administrative inquiries should be sent to the supervisors - namely Steve
Blackburn (steve.blackburn@anu.edu.au) and Tony Hosking
(antony.hosking@anu.edu.au).